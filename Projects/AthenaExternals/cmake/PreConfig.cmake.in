# Copyright (C) 2002-2022 CERN for the benefit of the ATLAS collaboration
#
# Settings for any project using AthenaExternals.
#

# Set the C++ standard to use.
set( CMAKE_CXX_STANDARD @CMAKE_CXX_STANDARD@
   CACHE STRING "C++ standard used for the build" )
set( CMAKE_CXX_EXTENSIONS @CMAKE_CXX_EXTENSIONS@
   CACHE STRING "(Dis)Allow the usage of C++ extensions" )

# Use the -pthread flag for the build instead of the -lpthread linker option,
# whenever possible.
set( THREADS_PREFER_PTHREAD_FLAG TRUE CACHE BOOL
   "Prefer using the -pthread compiler flag over -lpthread" )

# Handle CMP0072 by actively selecting the legacy OpenGL library by default.
set( OpenGL_GL_PREFERENCE "LEGACY" CACHE BOOL
   "Prefer using /usr/lib64/libGL.so unless the user says otherwise" )

# By default do not consider virtual environments in Python's setup.
set( Python_FIND_VIRTUALENV "STANDARD" CACHE STRING
   "Do not use virtualenv or conda to set up Python" )
