Basic Linear Algebra Subprograms
================================

This package builds the BLAS library for the offline software of ATLAS.

The source code for BLAS is kept in this repository itself. At least for the
moment.
