GDB: The GNU Project Debugger
=============================

This package builds GDB for the offline software of ATLAS. Ensuring that
an appropriate GDB version would be available for the software development.

The source of GDB is taken from http://ftp.gnu.org/gnu/gdb/.
