APE
===

This package takes care of building APE for the offline software of ATLAS.

Currently the build takes the APE source code from
https://svn.cern.ch/reps/atlasoff/Offloading/APE. But the project will
likely move to a new location soon.
