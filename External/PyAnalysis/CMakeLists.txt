# Copyright (C) 2002-2023 CERN for the benefit of the ATLAS collaboration
#
# Configuration for building/installing python "analysis" modules.
#

# The name of the package:
atlas_subdir( PyAnalysis )

# Figure out whether we need to do anything.
if( NOT ATLAS_BUILD_PYANALYSIS )
   return()
endif()

# Tell the user what's happening:
message( STATUS "Building python (analysis) modules as part of this project" )

# Silence ExternalProject warnings with CMake >=3.24.
if( POLICY CMP0135 )
   cmake_policy( SET CMP0135 NEW )
endif()

# Figure out where to take Python from:
if( ATLAS_BUILD_PYTHON )
   set( Python_EXECUTABLE $<TARGET_FILE:Python::Interpreter> )
   set( Python_VERSION_MAJOR 3 )
   set( Python_VERSION_MINOR 9 )
else()
   find_package( Python COMPONENTS Interpreter REQUIRED )
   find_package( libffi REQUIRED )
endif()

# Install the find module(s):
install( FILES "cmake/FindNumPy.cmake"
   DESTINATION "${CMAKE_INSTALL_CMAKEDIR}/modules" )

# Setup the build/runtime environment for the python analysis packages:
configure_file(
   "${CMAKE_CURRENT_SOURCE_DIR}/cmake/PyAnalysisEnvironmentConfig.cmake.in"
   "${CMAKE_CURRENT_BINARY_DIR}${CMAKE_FILES_DIRECTORY}/PyAnalysisEnvironmentConfig.cmake"
   @ONLY )
set( PyAnalysisEnvironment_DIR
   "${CMAKE_CURRENT_BINARY_DIR}${CMAKE_FILES_DIRECTORY}"
   CACHE INTERNAL "Location of PyAnalysisEnvironmentConfig.cmake" )
find_package( PyAnalysisEnvironment )

# A common installation directory for all python externals of the package:
set( _buildDir
   "${CMAKE_CURRENT_BINARY_DIR}${CMAKE_FILES_DIRECTORY}/PyAnalysisBuild" )
set( _sitePkgsDir
   "${_buildDir}/lib/python${Python_VERSION_MAJOR}.${Python_VERSION_MINOR}/site-packages" )

# Environment setup for the build/installation of the python modules.
set( _pythonModuleBuildEnv "${CMAKE_COMMAND}" -E env
   --unset=SHELL
   PYTHONPATH=${_sitePkgsDir}
   LD_LIBRARY_PATH=$<TARGET_FILE_DIR:libffi::ffi>:$ENV{LD_LIBRARY_PATH}
   DYLD_LIBRARY_PATH=$<TARGET_FILE_DIR:libffi::ffi>:$ENV{DYLD_LIBRARY_PATH} )
set( _pythonModuleInstallArgs --root=/ --prefix=${_buildDir} )

# Build/install setuptools:
set( ATLAS_SETUPTOOLS_SOURCE
   "URL;http://cern.ch/lcgpackages/tarFiles/sources/setuptools-57.1.0.tar.gz;URL_MD5;8e0d477fb7f722ed5b6b7e84a627027d"
   CACHE STRING "The source for setuptools" )
mark_as_advanced( ATLAS_SETUPTOOLS_SOURCE )
ExternalProject_Add( setuptools
   PREFIX "${CMAKE_BINARY_DIR}"
   INSTALL_DIR "${CMAKE_BINARY_DIR}/${ATLAS_PLATFORM}"
   ${ATLAS_SETUPTOOLS_SOURCE}
   BUILD_IN_SOURCE 1
   CONFIGURE_COMMAND ${CMAKE_COMMAND} -E make_directory "${_sitePkgsDir}"
   COMMAND ${_pythonModuleBuildEnv}
   "${Python_EXECUTABLE}" "<SOURCE_DIR>/bootstrap.py"
   BUILD_COMMAND ${_pythonModuleBuildEnv}
   "${Python_EXECUTABLE}" "<SOURCE_DIR>/setup.py" build
   INSTALL_COMMAND ${_pythonModuleBuildEnv}
   "${Python_EXECUTABLE}" "<SOURCE_DIR>/setup.py" install
   ${_pythonModuleInstallArgs}
   COMMAND "${CMAKE_CURRENT_SOURCE_DIR}/../scripts/sanitizePythonScripts.sh"
   "${_buildDir}/bin/easy_install*"
   COMMAND ${CMAKE_COMMAND} -E copy_directory "${_buildDir}/" "<INSTALL_DIR>" )
add_dependencies( Package_PyAnalysis setuptools )
if( ATLAS_BUILD_PYTHON )
   add_dependencies( setuptools Python )
endif()

if( NOT NUMPY_FOUND OR ATLAS_BUILD_PYTHON )

   # Build/install Cython.
   set( ATLAS_CYTHON_SOURCE
      "URL;https://cern.ch/lcgpackages/tarFiles/sources/Cython-0.29.33.tar.gz;URL_MD5;bd42c555cb2298b8a94fa8de7ee679ba"
      CACHE STRING "The source for Cython" )
   mark_as_advanced( ATLAS_CYTHON_SOURCE )
   ExternalProject_Add( Cython
      PREFIX "${CMAKE_BINARY_DIR}"
      INSTALL_DIR "${CMAKE_BINARY_DIR}/${ATLAS_PLATFORM}"
      ${ATLAS_CYTHON_SOURCE}
      BUILD_IN_SOURCE 1
      CONFIGURE_COMMAND ${CMAKE_COMMAND} -E make_directory "${_sitePkgsDir}"
      BUILD_COMMAND ${_pythonModuleBuildEnv}
      "${Python_EXECUTABLE}" "<SOURCE_DIR>/setup.py" build
      INSTALL_COMMAND ${_pythonModuleBuildEnv}
      "${Python_EXECUTABLE}" "<SOURCE_DIR>/setup.py" install
      ${_pythonModuleInstallArgs}
      COMMAND ${CMAKE_COMMAND} -E copy_directory "${_buildDir}/" "<INSTALL_DIR>" )
   add_dependencies( Package_PyAnalysis Cython )
   add_dependencies( Cython setuptools )

   # Build/install numpy:
   set( ATLAS_NUMPY_SOURCE
      "URL;http://cern.ch/lcgpackages/tarFiles/sources/numpy-1.23.5.tar.gz;URL_MD5;8b2692a511a3795f3af8af2cd7566a15"
      CACHE STRING "The source for NumPy" )
   mark_as_advanced( ATLAS_NUMPY_SOURCE )
   ExternalProject_Add( numpy
      PREFIX "${CMAKE_BINARY_DIR}"
      INSTALL_DIR "${CMAKE_BINARY_DIR}/${ATLAS_PLATFORM}"
      ${ATLAS_NUMPY_SOURCE}
      BUILD_IN_SOURCE 1
      CONFIGURE_COMMAND ${CMAKE_COMMAND} -E make_directory "${_sitePkgsDir}"
      BUILD_COMMAND ${_pythonModuleBuildEnv}
      "${Python_EXECUTABLE}" "<SOURCE_DIR>/setup.py" build
      INSTALL_COMMAND ${_pythonModuleBuildEnv}
      "${Python_EXECUTABLE}" "<SOURCE_DIR>/setup.py" install
      ${_pythonModuleInstallArgs}
      COMMAND "${CMAKE_CURRENT_SOURCE_DIR}/../scripts/sanitizePythonScripts.sh"
      "${_buildDir}/bin/f2py*"
      COMMAND ${CMAKE_COMMAND} -E copy_directory "${_buildDir}/" "<INSTALL_DIR>" )
   add_dependencies( Package_PyAnalysis numpy )
   add_dependencies( numpy setuptools Cython )
endif()

# Build/install pyyaml.
set( ATLAS_PYYAML_SOURCE
   "URL;http://cern.ch/lcgpackages/tarFiles/sources/PyYAML-6.0.tar.gz;URL_MD5;1d19c798f25e58e3e582f0f8c977dbb8"
   CACHE STRING "The source for PyYAML" )
mark_as_advanced( ATLAS_PYYAML_SOURCE )
ExternalProject_Add( PyYAML
   PREFIX "${CMAKE_BINARY_DIR}"
   INSTALL_DIR "${CMAKE_BINARY_DIR}/${ATLAS_PLATFORM}"
   ${ATLAS_PYYAML_SOURCE}
   BUILD_IN_SOURCE 1
   CONFIGURE_COMMAND ${CMAKE_COMMAND} -E make_directory "${_sitePkgsDir}"
   BUILD_COMMAND ${_pythonModuleBuildEnv}
   "${Python_EXECUTABLE}" "<SOURCE_DIR>/setup.py" build
   INSTALL_COMMAND ${_pythonModuleBuildEnv}
   "${Python_EXECUTABLE}" "<SOURCE_DIR>/setup.py" install
   ${_pythonModuleInstallArgs}
   COMMAND ${CMAKE_COMMAND} -E copy_directory "${_buildDir}/" "<INSTALL_DIR>" )
add_dependencies( Package_PyAnalysis PyYAML )
add_dependencies( PyYAML setuptools )

if( NOT PIP_FOUND OR ATLAS_BUILD_PYTHON )

   # Build/install wheel:
   set( ATLAS_WHEEL_SOURCE
      "URL;http://cern.ch/lcgpackages/tarFiles/sources/wheel-0.37.1.tar.gz;URL_MD5;f490f1399e5903706cb1d4fbed9ecb28"
      CACHE STRING "The source for wheel" )
   mark_as_advanced( ATLAS_WHEEL_SOURCE )
   ExternalProject_Add( wheel
      PREFIX "${CMAKE_BINARY_DIR}"
      INSTALL_DIR "${CMAKE_BINARY_DIR}/${ATLAS_PLATFORM}"
      ${ATLAS_WHEEL_SOURCE}
      BUILD_IN_SOURCE 1
      CONFIGURE_COMMAND ${CMAKE_COMMAND} -E make_directory "${_sitePkgsDir}"
      BUILD_COMMAND ${_pythonModuleBuildEnv}
      "${Python_EXECUTABLE}" "<SOURCE_DIR>/setup.py" build
      INSTALL_COMMAND ${_pythonModuleBuildEnv}
      "${Python_EXECUTABLE}" "<SOURCE_DIR>/setup.py" install
      ${_pythonModuleInstallArgs}
      COMMAND "${CMAKE_CURRENT_SOURCE_DIR}/../scripts/sanitizePythonScripts.sh"
      "${_buildDir}/bin/wheel*"
      COMMAND ${CMAKE_COMMAND} -E copy_directory "${_buildDir}/" "<INSTALL_DIR>" )
   add_dependencies( Package_PyAnalysis wheel )
   add_dependencies( wheel setuptools )
   if( ATLAS_BUILD_PYTHON )
      add_dependencies( wheel Python )
   endif()
   # This makes sure that package compilation would happen one-by-one. Otherwise
   # some race conditions can arise when installing packages into the same area
   # at once.
   if( NOT NUMPY_FOUND OR ATLAS_BUILD_PYTHON )
      add_dependencies( wheel numpy )
   endif()

   # Build/install pip:
   set( ATLAS_PIP_SOURCE
      "URL;http://cern.ch/lcgpackages/tarFiles/sources/pip-22.0.4.tar.gz;URL_MD5;ffb2a7aa43004601409b3318777b75a8"
      CACHE STRING "The source for pip" )
   mark_as_advanced( ATLAS_PIP_SOURCE )
   ExternalProject_Add( pip
      PREFIX "${CMAKE_BINARY_DIR}"
      INSTALL_DIR "${CMAKE_BINARY_DIR}/${ATLAS_PLATFORM}"
      ${ATLAS_PIP_SOURCE}
      BUILD_IN_SOURCE 1
      CONFIGURE_COMMAND ${CMAKE_COMMAND} -E make_directory "${_sitePkgsDir}"
      BUILD_COMMAND ${_pythonModuleBuildEnv}
      "${Python_EXECUTABLE}" "<SOURCE_DIR>/setup.py" build
      INSTALL_COMMAND ${_pythonModuleBuildEnv}
      "${Python_EXECUTABLE}" "<SOURCE_DIR>/setup.py" install
      ${_pythonModuleInstallArgs}
      COMMAND "${CMAKE_CURRENT_SOURCE_DIR}/../scripts/sanitizePythonScripts.sh"
      "${_buildDir}/bin/pip*"
      COMMAND ${CMAKE_COMMAND} -E copy_directory "${_buildDir}/" "<INSTALL_DIR>" )
   add_dependencies( Package_PyAnalysis pip )
   add_dependencies( pip setuptools wheel )
   if( ATLAS_BUILD_PYTHON )
      add_dependencies( pip Python )
   endif()
endif()

# Set up a dummy imported target that other packages could use to find these
# python modules in their own builds.
add_library( PyAnalysis::PyAnalysis UNKNOWN IMPORTED GLOBAL )
set_target_properties( PyAnalysis::PyAnalysis PROPERTIES
   INSTALL_PATH "${_buildDir}"
   BINARY_PATH "${_buildDir}/bin"
   PYTHON_PATH "${_sitePkgsDir}" )

# Install all built modules at the same time:
install( DIRECTORY "${_buildDir}/"
   DESTINATION . USE_SOURCE_PERMISSIONS OPTIONAL )

# Test that, whatever the setup, we are able to use pip to install (as an
# example) the cookiecutter project.
set( "_pipInstallDir"
   "${CMAKE_CURRENT_BINARY_DIR}${CMAKE_FILES_DIRECTORY}/cookiecutterTest" )
atlas_add_test( pip_install
   SCRIPT pip install --target=${_pipInstallDir} cookiecutter )
