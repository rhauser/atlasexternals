// Copyright (C) 2002-2022 CERN for the benefit of the ATLAS collaboration.
// thread23_test: testing check_pointer_write_from_const_member

#pragma ATLAS check_thread_safety

namespace std {
template <class T>
class unique_ptr
{
public:
  T& operator* [[ATLAS::not_const_thread_safe]] () const { return *m_p; }
  T* get [[ATLAS::not_const_thread_safe]] () const { return m_p; }
  T* release [[ATLAS::not_const_thread_safe]] () { return m_p; }
private:
  T* m_p;
};
}

struct MyTool {
  void doIt1() const;
  void doIt2();
  int* m_p;
  std::unique_ptr<int> m_up;
};

void MyTool::doIt1() const
{
  *m_p = 1;  // warning
  int* p1 = m_p;
  *p1 = 2; // warning
  int* p2 [[ATLAS::thread_safe]] = m_p;
  *p2 = 2; // no warning
  *m_up = 3; // warning
  int* p3 = m_up.get();
  *p3 = 2; // warning
  int* p4 [[ATLAS::thread_safe]] = m_up.get();
  *p4 = 2; // no warning
}


void MyTool::doIt2() // no warnings
{
  *m_p = 1;
  int* p1 = m_p;
  *p1 = 2;
  *m_up = 3;
  int* p3 = m_up.get();
  *p3 = 2;
}
