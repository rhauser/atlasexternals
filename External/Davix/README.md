Davix HTTP Based File Access
============================

The [davix project](https://dmc.web.cern.ch/projects/davix/home)
aims to make file management over HTTP-based protocols simple.
The focus is on high-performance remote I/O and data management of
large collections of files. Currently, there is support for the
[WebDav](https://en.wikipedia.org/wiki/WebDAV),
[Amazon S3](https://en.wikipedia.org/wiki/Amazon_S3),
[Microsoft Azure](https://azure.microsoft.com/), and
[HTTP](https://en.wikipedia.org/wiki/Hypertext_Transfer_Protocol) protocols.

Click [here](https://dmc-docs.web.cern.ch/dmc-docs/versions/docs/davix-epel/html/)
to visit the davix documentation of the latest released version.
