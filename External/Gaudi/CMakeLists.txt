# Copyright (C) 2002-2023 CERN for the benefit of the ATLAS collaboration
#
# Package building Gaudi as part of an externals project.
#

# Minimum CMake version needed by the Gaudi configuration.
cmake_minimum_required( VERSION 3.15 )

# Silence ExternalProject warnings with CMake >=3.24.
if( POLICY CMP0135 )
   cmake_policy( SET CMP0135 NEW )
endif()

# Set the name of the package.
atlas_subdir( Gaudi )

# Declare where to get Gaudi from.
set( ATLAS_GAUDI_SOURCE
   "URL;https://gitlab.cern.ch/atlas/Gaudi/-/archive/v36r14.000/Gaudi-v36r14.000.tar.gz;URL_MD5;1cc75461a7cdc87daf1f9b6cb48725b4"
   CACHE STRING "The source for Gaudi" )
mark_as_advanced( ATLAS_GAUDI_SOURCE )

# Decide whether / how to patch the Gaudi sources.
set( ATLAS_GAUDI_PATCH "" CACHE STRING "Patch command for Gaudi" )
set( ATLAS_GAUDI_FORCEDOWNLOAD_MESSAGE
   "Forcing the re-download of Gaudi (2023.08.16.)"
   CACHE STRING "Download message to update whenever patching changes" )
mark_as_advanced( ATLAS_GAUDI_PATCH ATLAS_GAUDI_FORCEDOWNLOAD_MESSAGE )

# Assemble the CMake configuration arguments for Gaudi for its required
# dependencies.
set( _cmakeArgs )
if( ATLAS_BUILD_BOOST )
   list( APPEND _cmakeArgs
      -DBoost_ROOT:PATH=${CMAKE_BINARY_DIR}/${ATLAS_PLATFORM} )
else()
   find_package( Boost CONFIG )
   list( APPEND _cmakeArgs
      -DBoost_ROOT:PATH=${Boost_DIR} )
endif()
if( ATLAS_BUILD_PYTHON )
   list( APPEND _cmakeArgs
      -DPython_EXECUTABLE:FILEPATH=${CMAKE_BINARY_DIR}/${ATLAS_PLATFORM}/${CMAKE_INSTALL_BINDIR}/python
      -DPython_LIBRARY:FILEPATH=${CMAKE_BINARY_DIR}/${ATLAS_PLATFORM}/${CMAKE_INSTALL_LIBDIR}/${CMAKE_SHARED_LIBRARY_PREFIX}Python${CMAKE_SHARED_LIBRARY_SUFFIX}
      -DPython_INCLUDE_DIR:PATH=${CMAKE_BINARY_DIR}/${ATLAS_PLATFORM}/${CMAKE_INSTALL_INCLUDEDIR} )
else()
   find_package( Python COMPONENTS Interpreter Development )
   list( APPEND _cmakeArgs
      -DPython_EXECUTABLE:FILEPATH=${Python_EXECUTABLE}
      -DPython_LIBRARY:FILEPATH=${Python_LIBRARY}
      -DPython_INCLUDE_DIR:PATH=${Python_INCLUDE_DIRS} )
endif()
list( APPEND _cmakeArgs -DGAUDI_USE_PYTHON_MAJOR:STRING=3 )
if( ATLAS_BUILD_ROOT )
   list( APPEND _cmakeArgs
      -DROOT_ROOT:PATH=${CMAKE_BINARY_DIR}/${ATLAS_PLATFORM} )
else()
   find_package( ROOT )
   find_package( Vc )
   find_package( VDT )
   list( APPEND _cmakeArgs -DROOT_ROOT:PATH=${ROOT_INSTALL_PATH}
      -DVDT_INCLUDE_DIR:PATH=${VDT_INCLUDE_DIR}
      -DVDT_LIBRARY:FILEPATH=${VDT_vdt_LIBRARY} )
endif()
if( ATLAS_BUILD_TBB )
   list( APPEND _cmakeArgs
      -DTBB_ROOT:PATH=${CMAKE_BINARY_DIR}/${ATLAS_PLATFORM} )
else()
   find_package( TBB )
   list( APPEND _cmakeArgs -DTBB_ROOT:PATH=${TBB_INSTALL_PATH} )
endif()
if( ATLAS_BUILD_EIGEN )
   list( APPEND _cmakeArgs
      -DEIGEN3_INCLUDE_DIR:PATH=${CMAKE_BINARY_DIR}/${ATLAS_PLATFORM}/${CMAKE_INSTALL_INCLUDEDIR} )
else()
   find_package( Eigen )
   list( APPEND _cmakeArgs
      -DEIGEN3_INCLUDE_DIR:PATH=${EIGEN_INCLUDE_DIR} )
endif()
find_package( UUID )
list( APPEND _cmakeArgs -DUUID_INCLUDE_DIR:PATH=${UUID_INCLUDE_DIR}
   -DUUID_LIBRARIES:PATH=${UUID_LIBRARIES} )
find_package( Threads )
find_package( ZLIB )
list( APPEND _cmakeArgs -DZLIB_INCLUDE_DIR:PATH=${ZLIB_INCLUDE_DIR}
   -DZLIB_LIBRARY:FILEPATH=${ZLIB_LIBRARY} )
find_package( Rangev3 )
list( APPEND _cmakeArgs
   -DRANGEV3_INCLUDE_DIR:PATH=${RANGEV3_INCLUDE_DIR} )
find_package( cppgsl )
list( APPEND _cmakeArgs -DCPPGSL_INCLUDE_DIR:PATH=${CPPGSL_INCLUDE_DIR} )
find_package( nose )
list( APPEND _cmakeArgs
   -Dnosetests_PROGRAM:FILEPATH=${NOSE_nosetests_EXECUTABLE} )
find_package( fmt )
list( APPEND _cmakeArgs -Dfmt_ROOT:PATH=${fmt_DIR} )
if( ATLAS_BUILD_NLOHMANN_JSON )
   list( APPEND _cmakeArgs
      -Dnlohmann_json_DIR:PATH=${CMAKE_BINARY_DIR}/${ATLAS_PLATFORM} )
else()
   find_package( nlohmann_json )
   list( APPEND _cmakeArgs -Dnlohmann_json_DIR:PATH=${nlohmann_json_DIR} )
endif()

# Assemble the CMake configuration arguments for Gaudi for its optional
# dependencies.
find_package( AIDA )
if( AIDA_FOUND )
   list( APPEND _cmakeArgs -DAIDA_INCLUDE_DIRS:PATH=${AIDA_INCLUDE_DIR} )
else()
   list( APPEND _cmakeArgs -DGAUDI_USE_AIDA:BOOL=FALSE )
endif()
if( ATLAS_BUILD_XERCESC )
   list( APPEND _cmakeArgs
      -DXercesC_INCLUDE_DIR:PATH=${CMAKE_BINARY_DIR}/${ATLAS_PLATFORM}/${CMAKE_INSTALL_INCLUDEDIR}
      -DXercesC_LIBRARY:FILEPATH=${CMAKE_BINARY_DIR}/${ATLAS_PLATFORM}/${CMAKE_INSTALL_LIBDIR}/${CMAKE_SHARED_LIBRARY_PREFIX}xerces-c${CMAKE_SHARED_LIBRARY_SUFFIX} )
else()
   find_package( XercesC )
   if( XERCESC_FOUND )
      list( APPEND _cmakeArgs
         -DXercesC_INCLUDE_DIR:PATH=${XercesC_INCLUDE_DIR}
         -DXercesC_LIBRARY:FILEPATH=${XercesC_LIBRARY} )
   else()
      list( APPEND _cmakeArgs -DGAUDI_USE_XERCESC:BOOL=FALSE )
   endif()
endif()
if( ATLAS_BUILD_CLHEP )
   list( APPEND _cmakeArgs
      -DCLHEP_ROOT:PATH=${CMAKE_BINARY_DIR}/${ATLAS_PLATFORM} )
else()
   find_package( CLHEP )
   if( CLHEP_FOUND )
      list( APPEND _cmakeArgs
         -DCLHEP_ROOT:PATH=${CLHEP_INSTALL_PATH} )
   else()
      list( APPEND _cmakeArgs -DGAUDI_USE_CLHEP:BOOL=FALSE )
   endif()
endif()
find_package( HepPDT )
if( HEPPDT_FOUND )
   list( APPEND _cmakeArgs
      -DHEPPDT_INCLUDE_DIR:PATH=${HEPPDT_INCLUDE_DIR}
      -DHEPPDT_PDT_LIBRARY:FILEPATH=${HEPPDT_HepPDT_LIBRARY}
      -DHEPPDT_PID_LIBRARY:FILEPATH=${HEPPDT_HepPID_LIBRARY} )
else()
   list( APPEND _cmakeArgs -DGAUDI_USE_HEPPDT:BOOL=FALSE )
endif()
find_package( CppUnit )
if( CPPUNIT_FOUND )
   list( APPEND _cmakeArgs
      -DCPPUNIT_INCLUDE_DIR:PATH=${CPPUNIT_INCLUDE_DIR}
      -DCPPUNIT_LIBRARY:PATH=${CPPUNIT_cppunit_LIBRARY} )
else()
   list( APPEND _cmakeArgs -DGAUDI_USE_CPPUNIT:BOOL=FALSE )
endif()
find_package( libunwind )
if( LIBUNWIND_FOUND )
   list( APPEND _cmakeArgs
      -DUNWIND_INCLUDE_DIR:PATH=${LIBUNWIND_INCLUDE_DIR}
      -DUNWIND_LIBRARIES:FILEPATH=${LIBUNWIND_unwind_LIBRARY} )
else()
   list( APPEND _cmakeArgs -DGAUDI_USE_UNWIND:BOOL=FALSE )
endif()

# Additional CMake configuration options.
if( NOT "${CMAKE_BUILD_TYPE}" STREQUAL "" )
    list( APPEND _cmakeArgs
      -DCMAKE_BUILD_TYPE:STRING=${CMAKE_BUILD_TYPE} )
endif()
if( "${CMAKE_CXX_STANDARD}" GREATER_EQUAL 11 )
   list( APPEND _cmakeArgs
      -DCMAKE_CXX_STANDARD:STRING=${CMAKE_CXX_STANDARD} )
endif()

# Directory for the temporary build results.
set( _buildDir ${CMAKE_CURRENT_BINARY_DIR}${CMAKE_FILES_DIRECTORY}/GaudiBuild )
# Directory holding the "stamp" files.
set( _stampDir ${CMAKE_CURRENT_BINARY_DIR}${CMAKE_FILES_DIRECTORY}/GaudiStamp )

# Set up the build of Gaudi.
ExternalProject_Add( Gaudi
   PREFIX "${CMAKE_BINARY_DIR}"
   INSTALL_DIR "${CMAKE_BINARY_DIR}/${ATLAS_PLATFORM}"
   STAMP_DIR "${_stampDir}"
   ${ATLAS_GAUDI_SOURCE}
   ${ATLAS_GAUDI_PATCH}
   CMAKE_CACHE_ARGS
   -DCMAKE_INSTALL_BINDIR:PATH=${CMAKE_INSTALL_BINDIR}
   -DCMAKE_INSTALL_LIBDIR:PATH=${CMAKE_INSTALL_LIBDIR}
   -DCMAKE_INSTALL_PREFIX:PATH=${_buildDir}
   ${_cmakeArgs}
   -DGAUDI_USE_GPERFTOOLS:BOOL=FALSE
   -DGAUDI_USE_DOXYGEN:BOOL=FALSE
   -DGAUDI_USE_INTELAMPLIFIER:BOOL=FALSE
   -DGAUDI_USE_JEMALLOC:BOOL=FALSE
   -DBUILD_TESTING:BOOL=FALSE
   CMAKE_COMMAND
   "${CMAKE_BINARY_DIR}${CMAKE_FILES_DIRECTORY}/atlas_build_run.sh"
   ${CMAKE_COMMAND}
   LOG_CONFIGURE ON )
ExternalProject_Add_Step( Gaudi forcedownload
   COMMAND ${CMAKE_COMMAND} -E echo "${ATLAS_GAUDI_FORCEDOWNLOAD_MESSAGE}"
   DEPENDERS download )
ExternalProject_Add_Step( Gaudi purgeBuild
   COMMAND ${CMAKE_COMMAND} -E remove_directory "<BINARY_DIR>"
   COMMAND ${CMAKE_COMMAND} -E make_directory "<BINARY_DIR>"
   COMMAND ${CMAKE_COMMAND} -E remove_directory "${_buildDir}"
   COMMENT "Removing previous build results for Gaudi"
   DEPENDEES download
   DEPENDERS patch )
ExternalProject_Add_Step( Gaudi forceconfigure
   COMMAND ${CMAKE_COMMAND} -E remove -f "<BINARY_DIR>/CMakeCache.txt"
   COMMENT "Forcing the configuration of Gaudi"
   DEPENDEES update
   DEPENDERS configure
   ALWAYS 1 )
ExternalProject_Add_Step( Gaudi buildinstall
   COMMAND ${CMAKE_COMMAND} -E copy_directory "${_buildDir}/" "<INSTALL_DIR>"
   COMMENT "Installing Gaudi into the build area"
   DEPENDEES install )
add_dependencies( Package_Gaudi Gaudi )
if( ATLAS_BUILD_BOOST )
   add_dependencies( Gaudi Boost )
endif()
if( ATLAS_BUILD_PYTHON )
   add_dependencies( Gaudi Python )
endif()
if( ATLAS_BUILD_ROOT )
   add_dependencies( Gaudi ROOT )
endif()
if( ATLAS_BUILD_TBB )
   add_dependencies( Gaudi TBB )
endif()
if( ATLAS_BUILD_EIGEN )
   add_dependencies( Gaudi Eigen )
endif()
if( ATLAS_BUILD_XERCESC )
   add_dependencies( Gaudi XercesC )
endif()
if( ATLAS_BUILD_CLHEP )
   add_dependencies( Gaudi CLHEP )
endif()
if( ATLAS_BUILD_NLOHMANN_JSON )
   add_dependencies( Gaudi nlohmann_json )
endif()

# Install Gaudi.
install( DIRECTORY "${_buildDir}/"
   DESTINATION . USE_SOURCE_PERMISSIONS OPTIONAL )
install( FILES "cmake/FindGaudi.cmake"
   DESTINATION "${CMAKE_INSTALL_CMAKEDIR}/modules" )
