ATLAS Externals Repository
==========================

This repository holds the code for building all flavours of ATLAS
"externals" projects. Projects holding code not developed by ATLAS,
but used by the offline/simulation/analysis software of the
experiment.

Code In The Repository
----------------------

The main project configurations can be found in the `Projects` directory.
You need to point CMake to one of those folders to build one of the
specific externals projects.

Other directories hold packages that either build, or set up some external
software to be used by downstream projects.

Build / Install
---------------

The projects can be built in a fairly standard way. The `AnalysisBaseExternals`
project should be possible to build on practically any POSIX platform that
supports C++14. All the other projects on the other hand need to be built
on some platform for which an LCG release is available.

You are highly recommended to use an out-of-source build for all projects,
even though in-source builds may also work. But they are generally not
supported/tested.

The most common build procedure is:

```bash
mkdir build
cd build/
cmake -DCMAKE_BUILD_TYPE=RelWithDebInfo -DCTEST_USE_LAUNCHERS=TRUE ../atlasexternals/Projects/AthenaExternals/
make
make install DESTDIR=/some/location
```

**N.B.**
- That you might need to do e.g. `asetup none,gcc8,cmakesetup` to get a recent compiler and a recent version of cmake.
- When building the latest HEAD revision, you might also need to explicitely set an even more recent version of CMake, by using, e.g., `asetup none,gcc8,cmakesetup --cmakeversion=3.17.3`



Version
-------

This branch of the repository is meant to serve as a basis for the builds of
the various projects of the [atlas/athena](https://gitlab.cern.ch/atlas/athena)
repository's main branch.
